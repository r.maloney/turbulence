#ifndef TILE_MAP_H
#define TILE_MAP_H

#include <SDL2/SDL_render.h>
#include <SDL2/SDL.h>

#include "texture_cache.h"

#define MAX_LAYERS 3
#define MAX_TILESETS 3

typedef struct {
    unsigned short tilesetIndex;
    SDL_Point textureOffset;
} Tile;

typedef struct {
    unsigned short widthTiles;
    unsigned short heightTiles;
    unsigned short tileWidth;
    unsigned short tileHeight;
    unsigned int layerCount;
    unsigned int tilesetCount;
    Texture tilesets[MAX_TILESETS];
    Tile* layers;
} TileMap;

TileMap* create(const char* tileMapFileName);

void destroy(TileMap* tileMap);

void render(SDL_Renderer* renderer, TileMap const* tileMap);

#endif //TILE_MAP_H