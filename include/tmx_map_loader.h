#ifndef TMX_MAP_LOADER_H
#define TMX_MAP_LOADER_H

#include "tile_map.h"

TileMap* loadFromFile(const char* tileMapFileName);

#endif //TMX_MAP_LOADER_H