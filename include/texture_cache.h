#ifndef TEXTURE_CACHE_H
#define TEXTURE_CACHE_H


typedef enum {
    WORLD
} Texture;

Texture textureFor(const char* filename);

#endif //TEXTURE_CACHE_H