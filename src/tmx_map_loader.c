#include "tmx_map_loader.h"
#include "texture_cache.h"

#include <expat.h>
#include <glib.h>
#include <zlib.h>
#include <stdbool.h>
#include <tile_map.h>
#include <assert.h>

//Per Zlib manual, "ugly hack" for Win support
#if defined(MSDOS) || defined(OS2) || defined(WIN32) || defined(__CYGWIN__)
#  include <fcntl.h>
#  include <io.h>
#  define SET_BINARY_MODE(file) setmode(fileno(file), O_BINARY)
#else
#  define SET_BINARY_MODE(file)
#endif

/** Internal Structures */
typedef struct {
    unsigned short layerWidth;
    unsigned short layerHeight;
    unsigned int* tileGids;
    bool compression;
    bool encoding;
} layer_data;

typedef struct {
    unsigned int firstGid;
    unsigned short tileWidth;
    unsigned short tileHeight;
    char* imageName;
    unsigned int imageWidth;
    unsigned int imageHeight;
} tileset_data;

/** Internal Functions */
static void start_element(void* data, const char* element, const char** attribute);

static void end_element(void* data, const char* el);

static void handle_data(void* data, const char* content, int length);

static void initialize();

static void populateTileMap(TileMap** tileMap);

static void cleanUp();

/** Internal Variables */
static bool processingLayer;

static bool processingTileSet;

static int lastLayer;

static int lastTileset;

static unsigned short mapWidth;

static unsigned short mapHeight;

static unsigned short mapTileWidth;

static unsigned short mapTileHeight;

static layer_data layers[MAX_LAYERS];

static tileset_data tilesets[MAX_TILESETS];

TileMap* loadFromFile(const char* tileMapFileName) {
    TileMap* result = NULL;

    initialize();

    FILE* fp;
    fp = fopen(tileMapFileName, "r");
    fseek(fp, 0, SEEK_END);
    long fSize = ftell(fp);
    rewind(fp);
    char* fileBuffer = malloc(fSize + 1);
    assert(NULL != fileBuffer);
    int chunksRead = fread(fileBuffer, fSize, 1, fp);
    assert(1 == chunksRead);
    fileBuffer[fSize] = '\0';
    fclose(fp);

    if (fp) {
        XML_Parser parser = XML_ParserCreate(NULL);
        XML_SetElementHandler(parser, start_element, end_element);
        XML_SetCharacterDataHandler(parser, handle_data);

        if (XML_STATUS_ERROR == XML_Parse(parser, fileBuffer, strlen(fileBuffer), XML_TRUE)) {
            printf("Error: %s\n", XML_ErrorString(XML_GetErrorCode(parser)));
        }

        XML_ParserFree(parser);
    } else {
        fprintf(stderr, "Unable to open file %s\n", tileMapFileName);
    }

    free(fileBuffer);

    populateTileMap(&result);

    fprintf(stdout, "Layer count check %d", result->layerCount);

    cleanUp();

    return result;
}

void start_element(void* data, const char* element, const char** attribute) {
    if (0 == strncmp("map", element, 3)) {
        for (int i = 0; attribute[i]; i += 2) {
            if (0 == strncmp("width", attribute[i], 5)) {
                mapWidth = atoi(attribute[i + 1]);
            } else if (0 == strncmp("height", attribute[i], 6)) {
                mapHeight = atoi(attribute[i + 1]);
            } else if (0 == strncmp("tilewidth", attribute[i], 9)) {
                mapTileWidth = atoi(attribute[i + 1]);
            } else if (0 == strncmp("tileheight", attribute[i], 10)) {
                mapTileHeight = atoi(attribute[i + 1]);
            }
        }
    } else if (0 == strncmp("tileset", element, 7)) {
        if (lastTileset < MAX_TILESETS) {
            for (int i = 0; attribute[i]; i += 2) {
                processingTileSet = true;
                if (0 == strncmp("tilewidth", attribute[i], 9)) {
                    tilesets[lastTileset].tileWidth = atoi(attribute[i + 1]);
                } else if (0 == strncmp("tileheight", attribute[i], 10)) {
                    tilesets[lastTileset].tileHeight = atoi(attribute[i + 1]);
                } else if (0 == strncmp("firstgid", attribute[i], 8)) {
                    tilesets[lastTileset].firstGid = atoi(attribute[i + 1]);
                }
            }
        }
    } else if (processingTileSet && 0 == strncmp("image", element, 5)) {
        if (lastTileset < MAX_TILESETS) {
            for (int i = 0; attribute[i]; i += 2) {
                if (0 == strncmp("source", attribute[i], 6)) {
                    const char* source = attribute[i + 1];
                    char* copy = malloc(sizeof(char) * (strlen(source) + 1) );
                    strcpy(copy, source);
                    tilesets[lastTileset].imageName = copy;
                } else if (0 == strncmp("height", attribute[i], 6)) {
                    tilesets[lastTileset].imageHeight = atoi(attribute[i + 1]);
                } else if (0 == strncmp("width", attribute[i], 5)) {
                    tilesets[lastTileset].imageWidth = atoi(attribute[i + 1]);
                }
            }
        }
    } else if (0 == strncmp("layer", element, 5)) {
        processingLayer = true;
        if (lastLayer < MAX_LAYERS) {
            for (int i = 0; attribute[i]; i += 2) {
                if (0 == strncmp("width", attribute[i], 5)) {
                    layers[lastLayer].layerWidth = atoi(attribute[i + 1]);
                } else if (0 == strncmp("height", attribute[i], 6)) {
                    layers[lastLayer].layerHeight = atoi(attribute[i + 1]);
                }
            }
        }
    } else if (processingLayer && 0 == strncmp("data", element, 4)) {
        for (int i = 0; attribute[i]; i += 2) {
            if (0 == strncmp("encoding", attribute[i], 8)
                && 0 == strncmp("base64", attribute[i + 1], 6)) {
                layers[lastLayer].encoding = 1;
            } else if (0 == strncmp("compression", attribute[i], 11)
                       && 0 == strncmp("zlib", attribute[i + 1], 5)) {
                layers[lastLayer].compression = 1;
            }
        }
    }
}

void end_element(void* data, const char* el) {
    if (0 == strncmp("layer", el, 5)) {
        processingLayer = false;
        lastLayer++;
    } else if (0 == strncmp("tileset", el, 6)) {
        processingTileSet = false;
        lastTileset++;
    }
}

void handle_data(void* data, const char* content, int length) {
    char* tmp = malloc(length + 1);
    strncpy(tmp, content, length);
    tmp[length] = '\0';

    char* trimmed = tmp;

    while (isspace(*trimmed)) {
        trimmed++;
    }

    char* end = trimmed + strlen(trimmed) - 1;
    while (end > trimmed && isspace(*end)) {
        end--;
    }

    *(end + 1) = '\0';

    if (strlen(trimmed) > 0 && 0 != strncmp("\n", trimmed, 1)) {
        layer_data* pTopLayer = &layers[lastLayer];
        if (pTopLayer->encoding && pTopLayer->compression) {
            gsize out_len;
            guchar* result = g_base64_decode(trimmed, &out_len);
            unsigned int layerAreaInTiles = pTopLayer->layerWidth * pTopLayer->layerHeight;
            unsigned long decompresedSize = sizeof(unsigned int) * (layerAreaInTiles);
            unsigned int* decompressedVal = malloc(decompresedSize);

            int decompressResult = uncompress((Bytef*) decompressedVal, &decompresedSize, result, out_len);

            if (Z_OK == decompressResult) {
                pTopLayer->tileGids = decompressedVal;
            } else {
                if (Z_MEM_ERROR == decompressResult) {
                    printf("Mem error occured when reading the stream.");
                } else if (Z_BUF_ERROR == decompressResult) {
                    printf("Buffer error occured when reading the stream.");
                } else if (Z_DATA_ERROR == decompressResult) {
                    printf("Data error occured when reading the stream.");
                }
            }

            free(result);
        } else {
            //TODO: Store unencoded/uncompressed result if needed
        }
    }

    free(tmp);
}

void populateTileMap(TileMap** tileMap) {
    assert(lastTileset <= MAX_TILESETS);

    printf("Map dimensions: %d x %d, %d x %d", mapWidth, mapHeight, mapTileWidth, mapTileHeight);

    *tileMap = malloc(sizeof(TileMap));

    (*tileMap)->tileHeight = mapTileHeight;
    (*tileMap)->tileWidth = mapTileWidth;
    (*tileMap)->layerCount = lastLayer;
    (*tileMap)->tilesetCount = lastTileset;
    (*tileMap)->widthTiles = mapTileWidth;
    (*tileMap)->heightTiles = mapTileHeight;
    (*tileMap)->layers = malloc(sizeof(Tile) * ((mapTileWidth * mapTileHeight) * lastLayer));

    unsigned int tileCountPerLayer = mapTileWidth * mapTileHeight;

    for (unsigned int l = 0; l < lastLayer; ++l) {
        for (unsigned int i = 0; i < tileCountPerLayer; i++) {
            Tile* tile = &((*tileMap)->layers[i + (tileCountPerLayer * l)]);
//            tile->textureOffset;
//            tile->tilesetIndex;
        }
    }

    for (unsigned int i = 0; i < lastTileset; ++i) {
        printf("Tileset %d, first GID: %d, tile dimensions (%d x %d), image (%s) dimensions (%d x %d)\n",
               i,
               tilesets[i].firstGid,
               tilesets[i].tileWidth,
               tilesets[i].tileHeight,
               tilesets[i].imageName,
               tilesets[i].imageWidth,
               tilesets[i].imageHeight);
        (*tileMap)->tilesets[i] = textureFor(tilesets[i].imageName);
    }

    for (unsigned int i = 0; i < lastLayer; ++i) {
        printf("Layer %d, compression %s, encoding %s, dimensions (%d x %d)\n",
               i,
               layers[i].compression ? "true" : "false",
               layers[i].encoding ? "true" : "false",
               layers[i].layerHeight,
               layers[i].layerWidth);
    }
}

static void initialize() {
    lastLayer = 0;
    lastTileset = 0;
    for (int i = 0; i < MAX_LAYERS; i++) {
        layers[i].tileGids = NULL;
        layers[i].compression = false;
        layers[i].encoding = false;
        layers[i].layerHeight = 0;
        layers[i].layerWidth = 0;
    }

    for (int i = 0; i < MAX_TILESETS; i++) {
        tilesets[i].firstGid = 0;
        tilesets[i].imageName = NULL;
        tilesets[i].tileWidth = 0;
        tilesets[i].tileHeight = 0;
        tilesets[i].imageWidth = 0;
        tilesets[i].imageHeight = 0;
    }
}

static void cleanUp() {
    for (int i = 0; i < lastLayer; i++) {
        free(layers[i].tileGids);
        layers[i].tileGids = NULL;
    }

    for (int i = 0; i < lastTileset; i++) {
        free(tilesets[i].imageName);
        tilesets[i].imageName = NULL;
    }
}