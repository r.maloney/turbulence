#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL_image.h>
#include "tile_map.h"


int main() {
    if (SDL_Init(SDL_INIT_AUDIO | SDL_INIT_VIDEO | SDL_INIT_TIMER) < 0) {
        fprintf(stderr, "Unable to init SDL: %s\n", SDL_GetError());
        exit(1);
    }

    SDL_Window *window = SDL_CreateWindow("indomitable",
            SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
            640, 480,
            SDL_WINDOW_SHOWN);

    if (!window) {
        fprintf(stderr, "Unable to create window with 640x480 resolution: %s\n", SDL_GetError());
        exit(1);
    }

    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    if (!renderer) {
        fprintf(stderr, "Unable to create renderer: %s\n", SDL_GetError());
        exit(1);
    }

    SDL_Surface* tempSurface = IMG_Load("assets/oryx_16bit_scifi_world_trans.png");

    if (!tempSurface) {
        fprintf(stderr, "Unable to load surface: %s\n", SDL_GetError());
        exit(1);
    }

    SDL_Texture* testTexture = SDL_CreateTextureFromSurface(renderer, tempSurface);
    SDL_FreeSurface(tempSurface);

    TileMap* tileMap = create("assets/landing_dome.tmx");

    SDL_Rect sourceRect;
    SDL_Rect destRect;

    sourceRect.x = destRect.x = 0;
    sourceRect.y = destRect.y = 0;
    sourceRect.w = destRect.w = 64;
    sourceRect.h = destRect.h = 64;

    SDL_Event e;
    while (1) {
        if (SDL_PollEvent(&e)) {
            if (SDL_QUIT == e.type) {
                break;
            }
        }

        SDL_RenderClear(renderer);
        render(renderer, tileMap);
        SDL_RenderCopy(renderer, testTexture, &sourceRect, &destRect);
        SDL_RenderPresent(renderer);
    }

    destroy(tileMap);

    SDL_DestroyTexture(testTexture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

    SDL_Quit();
}
