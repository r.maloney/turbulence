#include <tile_map.h>
#include <assert.h>
#include "tile_map.h"
#include "tmx_map_loader.h"

TileMap* create(const char* tileMapFileName) {
    return loadFromFile(tileMapFileName);
}

void destroy(TileMap* tileMap) {
    assert(NULL != tileMap);
    assert(NULL != tileMap->layers);
    free(tileMap->layers);
    free(tileMap);
}

void render(SDL_Renderer* renderer, TileMap const* tileMap) {

}